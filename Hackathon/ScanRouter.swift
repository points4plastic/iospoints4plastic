//
//  ScanRouter.swift
//  Hackathon
//
//  Created by Teodor Dermendjiev on 11/24/16.
//  Copyright © 2016 Plastic Points. All rights reserved.
//

import Foundation
import UIKit

class ScanRouter {
    static func instantiateInitialViewController() -> PRPQRScanViewController {
        
        let viewController = UIStoryboard(name: "Scan", bundle: nil).instantiateInitialViewController
        
        let vc : PFPQRScanViewController = viewController as! PRPQRScanViewController
        
        vc.viewModel = ScanViewModel()
        
        return navigationController
    }
    
    
}
