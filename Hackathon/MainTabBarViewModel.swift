//
//  MainTabBarViewModel.swift
//  Hackathon
//
//  Created by Teodor Dermendjiev on 11/24/16.
//  Copyright © 2016 Plastic Points. All rights reserved.
//

import UIKit

class MainTabBarViewModel: NSObject {
    
    
    //MARK :- Public Methods
    
    internal func p4pTabbarViewController ()  -> Array <UIViewController> {
        
        let homePage = UIViewController()
        
        return  [homePage]
    }
}
