//
//  PFPAuthViewController.swift
//  Hackathon
//
//  Created by Lyubomir Marinov on 11/24/16.
//  Copyright © 2016 Plastic Points. All rights reserved.
//

import UIKit
import PureForm

class PFPAuthViewController: UINavigationController, PFSegmentedControlDelegate, PFFormDelegate, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var authSwitch: UISegmentedControl!
    
    private var loginSettings: PFSettings!
    private var signupSettings: PFSettings!
    
    private var loginController: PFFormController!
    private var signupController: PFFormController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginSettings = PFSettings()
        loginSettings.isKeyboardTypeValidation = true
        loginSettings.formDelegate = self
        loginSettings.tableViewDelegate = self
        
        loginController = PFFormController(tableView: tableView, settings: loginSettings)
        
        signupSettings = PFSettings()
        signupSettings.isKeyboardTypeValidation = true
        signupSettings.formDelegate = self
        signupSettings.tableViewDelegate = self

        signupController = PFFormController(tableView: tableView, settings: signupSettings)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
