//
//  MainRouter.swift
//  Hackathon
//
//  Created by Teodor Dermendjiev on 11/24/16.
//  Copyright © 2016 Plastic Points. All rights reserved.
//

class MainRouter {
    static func instantiateInitialViewController() -> MainTabBarController {
        let viewController = MainTabBarController()
        return viewController
    }
}
