//
//  ScanViewModel.swift
//  Hackathon
//
//  Created by Teodor Dermendjiev on 11/24/16.
//  Copyright © 2016 Plastic Points. All rights reserved.
//

import Foundation

class ScanViewModel: ViewModel {
    
    var state = ViewModelState.Idle
    var willUpdateData: ((ScanViewModel) -> Void)? = nil
    var didUpdateData: ((ScanViewModel) -> Void)? = nil
    
    func update() {
        
    }
}
